// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

module chromeos.secure_channel.mojom;

enum ConnectionAttemptFailureReason {
  AUTHENTICATION_ERROR,
  GATT_CONNECTION_ERROR,
  REMOTE_DEVICE_INVALID_PUBLIC_KEY,
  REMOTE_DEVICE_INVALID_BEACON_SEEDS,
  REMOTE_DEVICE_INVALID_PSK,
  TIMEOUT_FINDING_DEVICE
};

// Delegate interface used to handle connection attempt successes/failures. Note
// that this interface is intended to be implemented by clients of the
// DeviceSync service.
interface ConnectionDelegate {
  // Invoked when a connection attempt failed (i.e., the failure occurred before
  // a connection could be established).
  OnConnectionAttemptFailure(ConnectionAttemptFailureReason reason);

  // TODO(khorimoto): Add a callback to handle when a connection has completed
  // successfully.
};
