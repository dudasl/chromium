#include "third_party/blink/renderer/core/paint/pdf_tags_stack.h"

#include <stack>
#include <iostream>

// These methods/variables are for testing purpose
// Stack of tags

std::stack<std::shared_ptr<SkPDFTag>>& GetPDFTagsStack() {
  static std::stack<std::shared_ptr<SkPDFTag>> pdfTagsStack_;

  return pdfTagsStack_;
}

std::shared_ptr<SkPDFTag> GetLastPDFTag() {
  if (GetPDFTagsStack().size() > 0)
    return GetPDFTagsStack().top();

  std::cerr << "Try to get tag from empty stack." << std::endl;
  return std::make_shared<SkPDFTag>();
}

void PushLastPDFTag(const std::shared_ptr<SkPDFTag>& tag) {
  GetPDFTagsStack().push(tag);
}

void PopLastPDFTag() {
  if (GetPDFTagsStack().size() > 0)
    GetPDFTagsStack().pop();
  else
    std::cerr << "Try to pop tag from empty stack." << std::endl;
}
