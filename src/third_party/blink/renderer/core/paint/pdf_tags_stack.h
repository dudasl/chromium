#ifndef THIRD_PARTY_BLINK_RENDERER_CORE_PAINT_PDF_TAGS_STACK_H_
#define  THIRD_PARTY_BLINK_RENDERER_CORE_PAINT_PDF_TAGS_STACK_H_

#include <memory>

#include "third_party/skia/include/core/SkPDFTag.h"

// Temporary functions. Should be removed later
std::shared_ptr<SkPDFTag> GetLastPDFTag();
void PushLastPDFTag(const std::shared_ptr<SkPDFTag>& tag);
void PopLastPDFTag();

#endif // THIRD_PARTY_BLINK_RENDERER_CORE_PAINT_PDF_TAGS_STACK_H_