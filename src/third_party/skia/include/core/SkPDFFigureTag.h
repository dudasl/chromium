/*
 * Copyright 2018 Foxit Inc.
 *
 */

#ifndef SkPDFFigureTag_DEFINED
#define SkPDFFigureTag_DEFINED

#include "SkPDFTag.h"

class SK_API SkPDFFigureTag : public SkPDFTag {
public:
    SkPDFFigureTag(const char* alternateText) : SkPDFTag("Figure"), alternateText_(alternateText) {}
    SkPDFFigureTag(const std::string&) : SkPDFTag("Figure"), alternateText_(alternateText) {}

private:
    std::string alternateText_;
};

#endif  // SkPDFFigureTag_DEFINED