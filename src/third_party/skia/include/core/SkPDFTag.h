/*
 * Copyright 2018 Foxit Inc.
 *
 */

#ifndef SkPDFTag_DEFINED
#define SkPDFTag_DEFINED

//#include "SkRefCnt.h"

#include <memory>
#include <ostream>
#include <stack>
#include <string>
#include <vector>

class /*SK_API*/ SkPDFTag /*: public SkRefCnt*/ {
public:
    SkPDFTag() {}
    SkPDFTag(const char* tagName, int mcid = -1)
            : tagName_(tagName != nullptr ? tagName : ""), mcid_(mcid) {}
    SkPDFTag(const std::string& tagName, int mcid = -1) : tagName_(tagName), mcid_(mcid) {}
    ~SkPDFTag() {}

    const std::string& GetTagName() const { return this->tagName_; }

    bool HasMCID() const { return this->mcid_ >= 0; }
    int GetMCID() const { return this->mcid_; }

    bool HasKids() const { return this->kids_.size() > 0; }
    std::size_t NumKids() const { return this->kids_.size(); }
    std::shared_ptr<SkPDFTag> GetKid(std::size_t pos) const {
        return this->kids_.size() > 0 ? this->kids_[pos] : std::make_shared<SkPDFTag>();
    }
    void AddKid(const std::shared_ptr<SkPDFTag>& kid) { this->kids_.push_back(kid); }
    void AddKid(std::shared_ptr<SkPDFTag>&& kid) { this->kids_.push_back(kid); }

    void Dump(std::ostream& stm, const std::string& indent = "") const;

private:
    //!< Holds tag name
    std::string tagName_;

    //!< Holds marked content identifier, -1 if tag do not represent leaf node (marked content)
    int mcid_ = -1;

    //!< Kids of this tag. For example <P> tag shuld have several <Span> tags as kids
    std::vector<std::shared_ptr<SkPDFTag>> kids_;

private:
    // Temporary to known last marked content identifier
    static int lastMCID_;
};

// Temporary functions. Should be removed later
std::shared_ptr<SkPDFTag> GetLastPDFTag();
void PushLastPDFTag(const std::shared_ptr<SkPDFTag>& tag);
void PopLastPDFTag();

#endif  // SkPDFTag_DEFINED
