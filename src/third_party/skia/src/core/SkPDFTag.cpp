/*
 * Copyright 2018 Foxit Inc.
 *
 */

#include "SkPDFTag.h"

#include <iostream>

void SkPDFTag::Dump(std::ostream& stm, const std::string& indent /* = ""*/) const {
    stm << this->GetTagName();
    if (this->HasMCID() == true) stm << " [MCID = " << this->GetMCID() << "]";
    stm << std::endl;

    std::size_t numKids = this->NumKids();
    for (std::size_t i = 0; i < numKids; i++) {
        stm << indent << "|-- ";
        this->GetKid(i)->Dump(stm, indent + (i < numKids - 1 ? "|   " : "    "));
    }
}

// These methods/variables are for testing purpose
// Stack of tags

std::stack<std::shared_ptr<SkPDFTag>> &GetPDFTagsStack() {
    static std::stack<std::shared_ptr<SkPDFTag>> pdfTagsStack_;

	return pdfTagsStack_;
}

int SkPDFTag::lastMCID_ = 0;

std::shared_ptr<SkPDFTag> GetLastPDFTag() {
    if (GetPDFTagsStack().size() > 0) return GetPDFTagsStack().top();

    std::cerr << "Try to get tag from empty stack." << std::endl;
    return std::make_shared<SkPDFTag>();
}

void PushLastPDFTag(const std::shared_ptr<SkPDFTag>& tag) { GetPDFTagsStack().push(tag); }

void PopLastPDFTag() {
    if (GetPDFTagsStack().size() > 0)
        GetPDFTagsStack().pop();
    else
        std::cerr << "Try to pop tag from empty stack." << std::endl;
}
