// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "chrome/browser/resource_coordinator/tab_features.h"

namespace resource_coordinator {

TabFeatures::TabFeatures() = default;

TabFeatures::~TabFeatures() = default;

TabFeatures::TabFeatures(const TabFeatures& other) = default;

}  // namespace resource_coordinator
