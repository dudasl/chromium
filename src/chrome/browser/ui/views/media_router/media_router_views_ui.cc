// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "chrome/browser/ui/views/media_router/media_router_views_ui.h"

namespace media_router {

MediaRouterViewsUI::MediaRouterViewsUI() = default;

MediaRouterViewsUI::~MediaRouterViewsUI() = default;

void MediaRouterViewsUI::UpdateSinks() {
  // TODO(crbug.com/826091): Implement this method.
}

}  // namespace media_router
